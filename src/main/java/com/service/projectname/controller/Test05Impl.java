package com.service.projectname.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-02-02T15:08:30.674+08:00")

@RestSchema(schemaId = "test05")
@RequestMapping(path = "/projectName", produces = MediaType.APPLICATION_JSON)
public class Test05Impl {

    @Autowired
    private Test05Delegate userTest05Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userTest05Delegate.helloworld(name);
    }

}
